import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class JFrameExemplo {

    public static void main(String[] args) {

        JFrame frame = new JFrame("Minha Janela");
        frame.setBounds(0, 0, 300, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());

        JLabel jLabel = new JLabel("Nome:");
        frame.add(jLabel);

        final JTextField jTextField = new JTextField(10);
        frame.add(jTextField);

        JButton jbutton = new JButton("OK!");
        frame.add(jbutton);

        final JLabel jLabelNome = new JLabel("Ola Mundo !!!!");
        frame.add(jLabelNome);

        jbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nome = jTextField.getText();
                jLabelNome.setText("Ola Mundo " + nome + " !!!!");
                jTextField.setText("");
                
                
            }
        }
        );

        frame.setVisible(true);


    }

}
